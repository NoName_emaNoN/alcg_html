$(function () {
    $('.b-splash-categories__item').on('click', function () {
        var href = $(this).data('href');

        if (href) {
            location.href = href;
        }
    });
});
