$(function () {
    $(window).on('resize orientationChange', function (event) {
        if (window.innerWidth > 667) {
            var max = 0;

            $('.b-places__item').each(function () {
                if ($(this).height() > max) {
                    max = $(this).height();
                }
            }).height(max);
        } else {
            $('.b-places__item').height('auto');
        }
    }).trigger('resize');
});
