$(function () {
    $('.b-structure__list').slick({
        arrows: false,
        dots: true,
        infinite: false,
        swipe: true,
        //centerMode: true,
        //centerPadding: '60px',
        slidesToShow: 4,
        responsive: [
            {
                breakpoint: 768 - 1,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            },
            //{
            //    breakpoint: 600,
            //    settings: {
            //        slidesToShow: 2,
            //        slidesToScroll: 2
            //    }
            //},
            //{
            //    breakpoint: 480,
            //    settings: {
            //        slidesToShow: 1,
            //        slidesToScroll: 1
            //    }
            //}
        ]
    });
});
